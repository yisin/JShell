package com.yisin.action;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

public class ServerIoHandler extends IoHandlerAdapter {

	@Override
	public void sessionOpened(IoSession iosession) throws Exception {
		System.out.println("会话连接已建立");
	}

	@Override
	public void messageReceived(IoSession iosession, Object obj) throws Exception {
		System.out.println("Server收到客户端数据：" + obj.toString());
		iosession.write("Hello,Client!");
	}

	@Override
	public void messageSent(IoSession iosession, Object obj) throws Exception {
		System.out.println("Serve端响应客户端发送数据:" + obj.toString());
	}

}
