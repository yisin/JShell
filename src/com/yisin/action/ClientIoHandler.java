package com.yisin.action;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

public class ClientIoHandler extends IoHandlerAdapter {

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		System.out.println("创建session连接");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void sessionClosed(IoSession iosession) throws Exception {
		
		System.out.println("会话连接关闭");
		iosession.close(true);
	}

	@Override
	public void messageReceived(IoSession iosession, Object obj) throws Exception {
		System.out.println("客户端成功接收到消息:" + obj.toString());
	}

	@Override
	public void messageSent(IoSession iosession, Object obj) throws Exception {

		System.out.println("客户端消息已发送成功，发送数据:" + obj.toString());

	}

}