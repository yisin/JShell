package com.yisin.action;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.nio.NioDatagramAcceptor;

public class UdpServer {

	public static void main(String[] args) throws Exception {
		NioDatagramAcceptor acceptor = new NioDatagramAcceptor();
		acceptor.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(new TextLineCodecFactory(Charset.forName("UTF-8"), LineDelimiter.WINDOWS.getValue(), LineDelimiter.WINDOWS.getValue())));

		acceptor.setHandler(new ServerIoHandler());
		acceptor.bind(new InetSocketAddress("127.0.0.1", 9897));
	}

}
