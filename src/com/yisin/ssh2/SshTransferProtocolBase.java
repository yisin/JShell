package com.yisin.ssh2;

public abstract class SshTransferProtocolBase extends SshBase implements ITransferProtocol {

	public SshTransferProtocolBase(String host, String user, String password) {
		super(host, user, password);
	}

	public SshTransferProtocolBase(String host, String user) {
		super(host, user);
	}

	public abstract void Get(String fromFilePath, String toFilePath) throws Exception;

	public abstract void Put(String fromFilePath, String toFilePath) throws Exception;

	public abstract void Mkdir(String directory) throws Exception;

	public abstract void Cancel() throws Exception;

	// Triggered when transfer is starting
	protected FileTransferEvent transferStart;
	// Triggered when transfer ends
	protected FileTransferEvent transferEnd;
	// Triggered on every interval with the transfer progress iformation.
	protected FileTransferEvent transferProgress;
	
	public void setOnTransferStart(FileTransferEvent transferStart) {
		this.transferStart = transferStart;
	}

	public void setOnTransferEnd(FileTransferEvent transferEnd) {
		this.transferEnd = transferEnd;
	}

	public void setOnTransferProgress(FileTransferEvent transferProgress) {
		this.transferProgress = transferProgress;
	}

	/**
	 * Sends a notification that a file transfer has started
	 * 
	 * @param src
	 *            The source file to transferred
	 * @param dst
	 *            Transfer destination
	 * @param totalBytes
	 *            Total bytes to transfer
	 * @param msg
	 *            A transfer message
	 */
	protected void SendStartMessage(String src, String dst, int totalBytes, String msg) {
		if (transferStart != null)
			transferStart.handle(src, dst, 0, totalBytes, msg);
	}

	/**
	 * Sends a notification that a file transfer has ended
	 * 
	 * @param src
	 *            The source file to transferred
	 * @param dst
	 *            Transfer destination
	 * @param transferredBytes
	 *            Transferred Bytes
	 * @param totalBytes
	 *            Total bytes to transfer
	 * @param msg
	 *            A transfer message
	 */
	protected void SendEndMessage(String src, String dst, int transferredBytes, int totalBytes, String msg) {
		if (transferEnd != null)
			transferEnd.handle(src, dst, transferredBytes, totalBytes, msg);
	}

	/**
	 * Sends a transfer progress notification
	 * 
	 * @param src
	 *            The source file to transferred
	 * @param dst
	 *            Transfer destination
	 * @param transferredBytes
	 *            Transferred Bytes
	 * @param totalBytes
	 *            Total bytes to transfer
	 * @param msg
	 *            A transfer message
	 **/
	protected void SendProgressMessage(String src, String dst, int transferredBytes, int totalBytes, String msg) {
		if (transferProgress != null) {
			transferProgress.handle(src, dst, transferredBytes, totalBytes, msg);
		}
	}
}
