package com.yisin.ssh2;

public interface ITransferProtocol {
	
	void Connect() throws Exception;

	void Close() throws Exception;

	void Cancel() throws Exception;

	void Get(String fromFilePath, String toFilePath) throws Exception;

	void Put(String fromFilePath, String toFilePath) throws Exception;

	void Mkdir(String directory) throws Exception;

}
