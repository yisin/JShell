package com.yisin.ssh2.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.yisin.ssh2.SeekOrigin;

public class CombinedStream {

	private InputStream m_in;
	private OutputStream m_out;

	public CombinedStream(InputStream inputStream, OutputStream outputStream) {
		this.m_in = inputStream;
		this.m_out = outputStream;
	}

	// / <summary>
	// / Reads a sequence of bytes from the current stream and advances the
	// position within the stream by the number of bytes read.
	// / </summary>
	// / <param name="buffer">An array of bytes. When this method returns, the
	// buffer contains the specified byte array with the values between offset
	// and (offset + count- 1) replaced by the bytes read from the current
	// source.</param>
	// / <param name="offset">The zero-based byte offset in buffer at which to
	// begin storing the data read from the current stream. </param>
	// / <param name="count">The maximum number of bytes to be read from the
	// current stream. </param>
	// / <returns>The total number of bytes read into the buffer. This can be
	// less than the number of bytes requested if that many bytes are not
	// currently available, or zero (0) if the end of the stream has been
	// reached.</returns>
	public int read(byte[] buffer, int offset, int count) throws IOException {
		return m_in.read(buffer, offset, count);
	}

	// / <summary>
	// / Reads a sequence of bytes from the current stream and advances the
	// position within the stream by the number of bytes read.
	// / </summary>
	// / <param name="buffer">An array of bytes. When this method returns, the
	// buffer contains the specified byte array with the values between offset
	// and (offset + count- 1) replaced by the bytes read from the current
	// source.</param>
	// / <returns>The total number of bytes read into the buffer. This can be
	// less than the number of bytes requested if that many bytes are not
	// currently available, or zero (0) if the end of the stream has been
	// reached.</returns>
	public int read(byte[] buffer) throws IOException {
		return read(buffer, 0, buffer.length);
	}
	
	public int readByte() throws IOException {
		byte[] buffer = new byte[1];
		read(buffer, 0, buffer.length);
		return buffer[0];
	}

	// / <summary>
	// / Writes a byte to the current position in the stream and advances the
	// position within the stream by one byte.
	// / </summary>
	// / <param name="value">The byte to write to the stream. </param>
	public void writeByte(byte value) throws IOException {
		m_out.write(new byte[] { value });
	}

	public void writeByte(int value) throws IOException {
		m_out.write((byte) value);
	}

	// / <summary>
	// / Writes a sequence of bytes to the current stream and advances the
	// current position within this stream by the number of bytes written.
	// / </summary>
	// / <param name="buffer">An array of bytes. This method copies count bytes
	// from buffer to the current stream. </param>
	// / <param name="offset">The zero-based byte offset in buffer at which to
	// begin copying bytes to the current stream.</param>
	// / <param name="count">The number of bytes to be written to the current
	// stream. </param>
	public void write(byte[] buffer, int offset, int count) throws IOException {
		m_out.write(buffer, offset, count);
	}

	// / <summary>
	// / Writes a sequence of bytes to the current stream and advances the
	// current position within this stream by the number of bytes written.
	// / </summary>
	// / <param name="buffer">An array of bytes. This method copies count bytes
	// from buffer to the current stream. </param>
	public void write(byte[] buffer) throws IOException {
		write(buffer, 0, buffer.length);
	}

	// / <summary>
	// / Gets a value indicating whether the current stream supports seeking.
	// This stream cannot seek, and will always return false.
	// / </summary>
	public boolean isCanSeek() {
		return false;
	}

	// / <summary>
	// / Clears all buffers for this stream and causes any buffered data to be
	// written to the underlying device.
	// / </summary>
	public void flush() throws IOException {
		m_out.flush();
	}

	// / <summary>
	// / Gets the length in bytes of the stream.
	// / </summary>
	public long getLength() {
		return 0;
	}

	// / <summary>
	// / Gets or sets the position within the current stream. This Stream cannot
	// seek. This property has no effect on the Stream and will always return 0.
	// / </summary>
	public long getPosition() {
		return 0;
	}

	// / <summary>
	// / This method has no effect on the Stream.
	// / </summary>
	public void SetLength(long value) {
	}

	// / <summary>
	// / This method has no effect on the Stream.
	// / </summary>
	public long Seek(long offset, SeekOrigin origin) {
		return 0;
	}

	public void Close() throws IOException {
		m_in.close();
		m_out.close();
	}

}
