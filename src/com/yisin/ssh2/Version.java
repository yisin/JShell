package com.yisin.ssh2;

import com.yinsin.utils.SystemUtils;

public class Version {

	private static String version = SystemUtils.getOsVersion();

	public String getVersion() {
		return version;
	}
	
	public String toString() {
		return version;
	}

}
