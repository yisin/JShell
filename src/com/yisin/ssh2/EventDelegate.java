package com.yisin.ssh2;

import java.util.LinkedList;
import java.util.List;

public class EventDelegate {

	private List<EventHandler> EVENTS = new LinkedList<EventHandler>();

	public boolean isActive(){
		return EVENTS.size() > 0;
	}
	
	/**
	 * 向事件中心广播一个事件，驱使事件中心执行该事件的处理器
	 * 
	 * @param param
	 */
	public void fire(Object... param) {
		for (EventHandler handler : EVENTS) {
			try {
				if (null != handler) {
					handler.handle(param);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 将自己注册为事件中心的某个事件的处理器
	 * 
	 * @param handler
	 */
	public void add(EventHandler handler) {
		EVENTS.add(handler);
	}

}
