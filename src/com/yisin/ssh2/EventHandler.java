package com.yisin.ssh2;

public abstract class EventHandler {

	public abstract void handle(Object... params);

}
