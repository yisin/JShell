package com.yisin.ssh2;

public class SshTransferException extends Exception {
	
	public SshTransferException(String msg){
		super(msg);
	}
	
	public SshTransferException(StringBuilder msg){
		super(msg.toString());
	}
	
}
