package com.yisin.ssh2;

public abstract interface Compression
{
  public static final int INFLATER = 0;
  public static final int DEFLATER = 1;

  public abstract void init(int paramInt1, int paramInt2);

  public abstract byte[] compress(byte[] paramArrayOfByte, int paramInt, int[] paramArrayOfInt);

  public abstract byte[] uncompress(byte[] paramArrayOfByte, int paramInt, int[] paramArrayOfInt);
}