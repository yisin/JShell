package com.yisin.ssh2;

public enum SeekOrigin {
	/** 指定流的开头。 */
	Begin(0),
	/** 指定流内的当前位置。 */
	Current(1),
	/** 指定流的结尾。 */
	End(2);

	public int index;

	SeekOrigin(int index) {
		this.index = index;
	}
}
