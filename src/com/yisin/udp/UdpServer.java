package com.yisin.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

public class UdpServer {
	private final static Logger logger = LoggerFactory.getLogger(UdpServer.class);
	private static final int ECHOMAX = 1024;
	private static final int PORT = 8657;
	private static DatagramSocket socket = null;
	private static Map<String, List<Map<String, Object>>> SoftUserMap = new HashMap<String, List<Map<String, Object>>>();

	public static void main(String[] args) {
		UdpServer.StartUdpServer();
	}

	public static Map<String, List<Map<String, Object>>> getSoftUser() {
		return SoftUserMap;
	}

	public static void StartUdpServer() {
		try {
			socket = new DatagramSocket(PORT);

			new UdpThread().start();
			new UdpCheckThread().start();
			new UDPClient().start();
		} catch (Exception e) {
			logger.error("启动Udp服务异常：" + e.getMessage());
		}
	}

	public static void PushMessage(String userId, JSONObject json) {
		try {
			List<Map<String, Object>> userList = SoftUserMap.get(userId);
			if (null != userList && !userList.isEmpty()) {
				Map<String, Object> userMap = userList.get(0);
				byte[] msgByte = new byte[ECHOMAX];
				byte[] bytes = null;
				DatagramPacket packet = (DatagramPacket) userMap.get("packet");
				logger.debug("push message to =>" + userMap);

				bytes = json.toJSONString().getBytes();
				msgByte = new byte[ECHOMAX];
				System.arraycopy(bytes, 0, msgByte, 0, bytes.length);

				packet.setData(msgByte);
				socket.send(packet);
			}
		} catch (Exception e) {
			logger.error("推送消息异常：" + e.getMessage());
		}
	}

	private static Map<String, Object> getUserMap(String userId, String addrees) {
		return getUserMap(SoftUserMap.get(userId), addrees);
	}

	private static Map<String, Object> getUserMap(List<Map<String, Object>> userList, String addrees) {
		String adr = "";
		if (null != userList && !userList.isEmpty()) {
			for (Map<String, Object> userMap : userList) {
				adr = (String) userMap.get("address");
				if (adr.equals(addrees)) {
					return userMap;
				}
			}
		}
		return null;
	}

	private static class UdpThread extends Thread {
		public void run() {
			String content = "", address = "";
			JSONObject json = null;
			DatagramPacket packet = new DatagramPacket(new byte[ECHOMAX], ECHOMAX);
			List<Map<String, Object>> userList = null;
			Map<String, Object> userMap = null;
			while (true) {
				try {
					socket.receive(packet);
					content = new String(packet.getData());
					json = JSONObject.parseObject(content);
					if(json.containsKey("userId")){
						address = packet.getSocketAddress().toString();
						if (SoftUserMap.containsKey(json.getString("userId"))) {
							userList = SoftUserMap.get(json.getString("userId"));
							userMap = getUserMap(userList, address);
							if (userMap == null) {
								userMap = new HashMap<String, Object>();
								userMap.put("time", System.currentTimeMillis());
								userMap.put("userId", json.getString("userId"));
								userMap.put("appName", json.getString("appName"));
								userMap.put("appVersion", json.getString("appVersion"));
								userMap.put("address", address);
								userMap.put("packet", packet);
								userList.add(userMap);
							}
							userMap.put("time", System.currentTimeMillis());
						} else {
							userMap = new HashMap<String, Object>();
							userMap.put("time", System.currentTimeMillis());
							userMap.put("userId", json.getString("userId"));
							userMap.put("appName", json.getString("appName"));
							userMap.put("appVersion", json.getString("appVersion"));
							userMap.put("address", address);
							userMap.put("packet", packet);
							
							userList = new ArrayList<Map<String, Object>>();
							userList.add(userMap);
							SoftUserMap.put(json.getString("userId"), userList);
						}
					}
				} catch (Exception e) {
					logger.error("接收客户端数据报时异常：" + e.getMessage());
				}
			}
		}
	}

	private static class UdpCheckThread extends Thread {

		public void run() {
			List<Map<String, Object>> userList = null;
			long start = 0, end = 0;
			Iterator<Map<String, Object>> it = null;
			Map<String, Object> userMap = null;
			String userId = null, address = null;
			while (true) {
				try {
					Thread.sleep(5000);
					end = System.currentTimeMillis();
					for (Map.Entry<String, List<Map<String, Object>>> entry : SoftUserMap.entrySet()) {
						userList = (List<Map<String, Object>>) entry.getValue();
						it = userList.iterator();
						while (it.hasNext()) {
							userMap = it.next();
							userId = (String) userMap.get("userId");
							address = (String) userMap.get("address");
							start = (Long) userMap.get("time");
							if ((end - start) > (1000 * 30)) {
								// 超时，删除
								logger.debug("Del ..... " + userId + " - " + address);
								it.remove();
							}
						}
					}
				} catch (Exception e) {
					logger.error("检测客户端连接时异常：" + e.getMessage());
				}
			}

		}

	}

	public static class UDPClient extends Thread {
		public void run() {
			try {
				DatagramSocket datagramSocket = new DatagramSocket();
				InetAddress address = InetAddress.getByName("127.0.0.1");
				byte[] bytes = "{}".getBytes();
				byte[] buffer = new byte[ECHOMAX];	
				System.arraycopy(bytes, 0, buffer, 0, bytes.length);
				DatagramPacket packet = null;
				while (true) {
					Thread.sleep(3000);
					// 发送数据
					packet = new DatagramPacket(buffer, buffer.length, address, PORT);
					datagramSocket.send(packet);
				}
			} catch (Exception e) {
				logger.error("Custom Client Send Error：" + e.getMessage());
			}
		}

	}

}
