package com.yisin.test;

import java.util.Scanner;

import com.yisin.ssh2.ShellMessageEvent;
import com.yisin.ssh2.SshShell;
import com.yisin.ssh2.jsch.Channel;
import com.yisin.ssh2.jsch.ChannelSftp;
import com.yisin.ssh2.jsch.Session;
import com.yisin.ssh2.jsch.SftpProgressMonitor;
import com.yisin.ssh2.jsch.UserInfo;

public class Test1 {

	private SshShell shell = null;
	private Session session = null;
	private Channel channel = null;
	private ChannelSftp sftp = null;
	
	private SshShell shell2 = null;
	private Session session2 = null;
	private Channel channel2 = null;
	private ChannelSftp sftp2 = null;

	public static void main(String[] args) {
		Test1 t = new Test1();
		t.init();
	}

	public void init() {

		try {
			// Create a new JSch instance

			// Prompt for username and server host
			String host = "192.168.20.32";
			String user = "gxyj";
			String pwd = "xxxx";

			shell = new SshShell(host, user, pwd);
			shell2 = new SshShell(host, user, pwd);

			shell.Connect(22);
			shell2.Connect(22);

			session = shell.getSession();
			session2 = shell.getSession();

			// Open a new Shell channel on the SSH session
			channel = shell.getChannel();
			sftp = (ChannelSftp) session.openChannel("sftp");
			sftp.connect();
			
			channel2 = shell2.getChannel();
			sftp2 = (ChannelSftp) session2.openChannel("sftp");
			sftp2.connect();

			System.out.println("-- Shell channel is connected using the {0} cipher" + session.getServerVersion());

			/*
			 * sftp.get("backup32.tar", "g:/backup32.tar", new
			 * SftpProgressMonitor(){
			 * 
			 * @Override public void init(int op, String src, String dest, long
			 * max) { System.out.println("------" + op + "--" + max); }
			 * 
			 * @Override public boolean count(long count) {
			 * System.out.println("-----------count------------" + count);
			 * return true; }
			 * 
			 * @Override public void end() {
			 * System.out.println("-----------end------------"); } });
			 * 
			 * sftp.put("g:/msg.wav", "/home/gxyj/msg.wav", new
			 * SftpProgressMonitor(){
			 * 
			 * @Override public void init(int op, String src, String dest, long
			 * max) { System.out.println("------" + op + "--" + max); }
			 * 
			 * @Override public boolean count(long count) {
			 * System.out.println("-----------count------------" + count);
			 * return true; }
			 * 
			 * @Override public void end() {
			 * System.out.println("-----------end------------"); } });
			 */
			
			channel.regEvent(new ShellMessageEvent() {
				public void handle(Object... params) {
					System.out.println("--111--->" + params[0]);
				}
			});
			
			channel2.regEvent(new ShellMessageEvent() {
				public void handle(Object... params) {
					System.out.println("--222--->" + params[0]);
				}
			});

			// Wait till channel is closed
			new Thread() {
				public void run() {
					try {
						System.out.println(channel.getClass().getName());
						Scanner scan = new Scanner(System.in);
						while (session.isConnected()) {
							System.out.println(channel.isClosed());
							
							while (!channel.isClosed()) {
								String read = scan.nextLine();
								System.out.println("===>" + read);
								if (read.equals("dis")) {
									channel.disconnect();
									
									Thread.sleep(1000);
									channel = shell.getChannel();
									channel = session.openChannel("shell");
									channel.connect();
									
									//sftp = (ChannelSftp) session.openChannel("sftp");
									//sftp.connect();
									break;
								} else {
									System.out.println("send===>" + read);
									channel.send(read);
									channel2.send(read);
								}
							}
							Thread.sleep(1000);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static class MyUserInfo implements UserInfo {
		private String passwd;

		@Override
		public String getPassphrase() {
			return null;
		}

		@Override
		public String getPassword() {
			return passwd;
		}

		@Override
		public boolean promptPassphrase(String str) {
			return true;
		}

		@Override
		public boolean promptPassword(String str) {
			passwd = str;
			return true;
		}

		@Override
		public boolean promptYesNo(String str) {
			return true;
		}

		@Override
		public void showMessage(String str) {
			System.out.println(str);
		}

	}

}
