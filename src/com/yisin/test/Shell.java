package com.yisin.test;

import com.yisin.ssh2.jsch.Channel;
import com.yisin.ssh2.jsch.JSch;
import com.yisin.ssh2.jsch.Session;

public class Shell {
	public static void main(String[] arg) {

		try {
			JSch jsch = new JSch();


			String host = "192.168.20.95";
			String user = "gxb";

			Session session = jsch.getSession(user, host, 22);

			session.setPassword("xxxx");

			session.setConfig("StrictHostKeyChecking", "no");

			session.connect(30000); // making a connection with timeout.

			Channel channel = session.openChannel("shell");

			// Enable agent-forwarding.
			// ((ChannelShell)channel).setAgentForwarding(true);

			channel.setInputStream(System.in);
			/*
			 * // a hack for MS-DOS prompt on Windows.
			 * channel.setInputStream(new FilterInputStream(System.in){ public
			 * int read(byte[] b, int off, int len)throws IOException{ return
			 * in.read(b, off, (len>1024?1024:len)); } });
			 */

			channel.setOutputStream(System.out);

			/*
			 * // Choose the pty-type "vt102".
			 * ((ChannelShell)channel).setPtyType("vt102");
			 */

			/*
			 * // Set environment variable "LANG" as "ja_JP.eucJP".
			 * ((ChannelShell)channel).setEnv("LANG", "ja_JP.eucJP");
			 */

			// channel.connect();
			channel.connect(3 * 1000);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
